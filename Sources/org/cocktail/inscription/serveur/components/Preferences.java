/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.inscription.serveur.components;

import java.io.IOException;
import java.net.UnknownHostException;

import org.cocktail.lecteurcheque.serveur.LecteurChequeClient;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.extensions.appserver.ERXRedirect;

public class Preferences extends MyComponent {
	// lecteur cheque
	private static final String CHEQUE_SERVER_HOSTNAME_CONFIG_KEY = "org.cocktail.scolarix.cheque.server.hostname";
	private static final String CHEQUE_SERVER_PORT_CONFIG_KEY = "org.cocktail.scolarix.cheque.server.port";
	private static final int DEFAULT_PORT = 2233;
	private String lecteurChequeServerHostName = null;
	private Integer lecteurChequeServerPort = null;

	public Preferences(WOContext context) {
		super(context);
	}

	public void reset() {
	}

	public Integer persIdUtilisateur() {
		return new Integer(session.connectedUserInfo().persId().intValue());
	}

	public WOActionResults validerParamsLecteurCheque() {
		try {
			if (session().getUtilisateur() != null) {
				session().getUtilisateur().setUtiChequeServerHostname(lecteurChequeServerHostName());
				session().getUtilisateur().setUtiChequeServerPort(lecteurChequeServerPort());
				session().defaultEditingContext().saveChanges();
				mySession().addSimpleInfoMessage("OK", "Préférences sauvegardées.");
				ERXRedirect redirectPage = new ERXRedirect(context());
				redirectPage.setComponent(session().getSavedPageWithName(Accueil.class.getName()));
				return redirectPage;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Erreur", "Problème pour enregistrer les préférences !");
		}
		return null;
	}

	public WOActionResults testLecteurCheque() {
		LecteurChequeClient lec = getNewLecteurChequeClient();
		if (lec != null) {
			mySession().addSimpleInfoMessage("OK",
					"Lecteur de chèque disponible sur " + lecteurChequeServerHostName() + ":" + lecteurChequeServerPort());
			try {
				lec.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private LecteurChequeClient getNewLecteurChequeClient() {
		try {
			return new LecteurChequeClient(null, lecteurChequeServerHostName(), lecteurChequeServerPort().intValue());
		}
		catch (UnknownHostException e1) {
			e1.printStackTrace();
			mySession().addSimpleErrorMessage("Problème", "Host " + lecteurChequeServerHostName() + " inconnu");
		}
		catch (IOException e2) {
			e2.printStackTrace();
			mySession().addSimpleErrorMessage("Problème",
					"Impossible de se connecter sur " + lecteurChequeServerHostName() + ":" + lecteurChequeServerPort());
		}
		catch (Exception e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Problème", e.getMessage());
		}
		return null;
	}

	public String lecteurChequeServerHostName() {
		if (lecteurChequeServerHostName == null) {
			if (session().getUtilisateur() != null) {
				lecteurChequeServerHostName = session().getUtilisateur().utiChequeServerHostname();
			}
			if (lecteurChequeServerHostName == null) {
				lecteurChequeServerHostName = myApp().config().stringForKey(CHEQUE_SERVER_HOSTNAME_CONFIG_KEY);
			}
		}
		return lecteurChequeServerHostName;
	}

	public void setLecteurChequeServerHostName(String lecteurChequeServerHostName) {
		this.lecteurChequeServerHostName = lecteurChequeServerHostName;
	}

	public Integer lecteurChequeServerPort() {
		if (lecteurChequeServerPort == null) {
			if (session().getUtilisateur() != null) {
				lecteurChequeServerPort = session().getUtilisateur().utiChequeServerPort();
			}
			if (lecteurChequeServerPort == null) {
				lecteurChequeServerPort = new Integer(myApp().config().intForKey(CHEQUE_SERVER_PORT_CONFIG_KEY));
			}
			if (lecteurChequeServerPort == null) {
				lecteurChequeServerPort = DEFAULT_PORT;
			}
		}
		return lecteurChequeServerPort;
	}

	public void setLecteurChequeServerPort(Integer lecteurChequeServerPort) {
		this.lecteurChequeServerPort = lecteurChequeServerPort;
	}

}