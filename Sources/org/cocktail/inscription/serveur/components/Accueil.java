/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.inscription.serveur.components;

import java.math.BigDecimal;

import org.cocktail.inscription.serveur.components.admissions.Admission;
import org.cocktail.inscription.serveur.components.outils.Editions;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.appserver.ERXRedirect;

public class Accueil extends MyComponent {

	public String searchValue;

	public Accueil(WOContext context) {
		super(context);
	}

	public String commentaires() {
		return "Bienvenue sur Inscription Administrative";
	}

	public void reset() {
		session().setErreur(null);
	}

	public NouveauDossier saisirNouveauDossier() {
		reset();
		NouveauDossier page;
		try {
			page = (NouveauDossier) session().getNewPageWithName(NouveauDossier.class.getName());
		}
		catch (Exception e) {
			page = (NouveauDossier) pageWithName(NouveauDossier.class.getName());
		}
		return page;
	}

	public WOComponent saisirNouveauDossierReInscription() {
		reset();
		NouveauDossierReInscription page;
		try {
			page = (NouveauDossierReInscription) session().getNewPageWithName(NouveauDossierReInscription.class.getName());
		}
		catch (Exception e) {
			page = (NouveauDossierReInscription) pageWithName(NouveauDossierReInscription.class.getName());
		}
		return page;
	}

	public WOComponent saisirNouveauDossierPreEtudiant() {
		reset();
		NouveauDossierPreEtudiant page;
		try {
			page = (NouveauDossierPreEtudiant) session().getNewPageWithName(NouveauDossierPreEtudiant.class.getName());
		}
		catch (Exception e) {
			page = (NouveauDossierPreEtudiant) pageWithName(NouveauDossierPreEtudiant.class.getName());
		}
		return page;
	}

	public WOComponent admission() {
		reset();
		Admission page;
		try {
			page = (Admission) session().getNewPageWithName(Admission.class.getName());
		}
		catch (Exception e) {
			page = (Admission) pageWithName(Admission.class.getName());
		}
		return page;
	}

	public WOComponent rechercheDossier() {
		reset();
		RechercheDossier page;
		try {
			page = (RechercheDossier) session().getSavedPageWithName(RechercheDossier.class.getName());
		}
		catch (Exception e) {
			page = (RechercheDossier) pageWithName(RechercheDossier.class.getName());
		}
		page.retourListeEtudiants();
		return page;
	}

	public WOActionResults rechercheDossierSearchValue() {
		reset();
		if (searchValue == null) {
			return null;
		}
		RechercheDossier page;
		try {
			page = (RechercheDossier) session().getSavedPageWithName(RechercheDossier.class.getName());
		}
		catch (Exception e) {
			page = (RechercheDossier) pageWithName(RechercheDossier.class.getName());
		}
		try {
			Integer etudNumero = Integer.valueOf(searchValue.trim());
			page.setQbe(new NSMutableDictionary<String, Object>(new BigDecimal(etudNumero), "etudNumero"));
		}
		catch (NumberFormatException e) {
			NSMutableDictionary<String, Object> qbeDico = new NSMutableDictionary<String, Object>();
			// qbeDico.takeValueForKey(new BigDecimal(session().anneeScolaire().fannDebut()), "annee");
			qbeDico.takeValueForKey(searchValue.trim(), "nomPatronymique");
			page.setQbe(qbeDico);
		}
		page.ctrl.rechercherLesEtudiants();
		if (page.ctrl.etudiants() == null || page.ctrl.etudiants().count() == 0) {
			return null;
		}
		ERXRedirect redirect = new ERXRedirect(context());
		redirect.setComponent(page);
		return redirect;
	}

	public WOComponent recherchePreDossier() {
		reset();
		RecherchePreDossier page;
		try {
			page = (RecherchePreDossier) session().getSavedPageWithName(RecherchePreDossier.class.getName());
		}
		catch (Exception e) {
			page = (RecherchePreDossier) pageWithName(RecherchePreDossier.class.getName());
		}
		page.retourListe();
		return page;
	}

	public WOComponent outils() {
		reset();
		Outils page;
		try {
			page = (Outils) session().getSavedPageWithName(Outils.class.getName());
		}
		catch (Exception e) {
			page = (Outils) pageWithName(Outils.class.getName());
		}
		return page;
	}

	public WOComponent editions() {
		reset();
		Editions page;
		try {
			page = (Editions) session().getSavedPageWithName(Editions.class.getName());
		}
		catch (Exception e) {
			page = (Editions) pageWithName(Editions.class.getName());
		}
		return page;
	}

	public WOComponent quitter() {
		reset();
		return session().logout();
	}
}