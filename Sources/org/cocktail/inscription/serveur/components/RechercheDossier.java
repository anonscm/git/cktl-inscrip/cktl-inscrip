/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.inscription.serveur.components;

import org.cocktail.inscription.serveur.controleurs.CtrlRechercheDossier;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSMutableDictionary;

public class RechercheDossier extends MyComponent {
	public CtrlRechercheDossier ctrl = null;

	private NSMutableDictionary<String, Object> qbe;

	private boolean isDossierBusy;

	public RechercheDossier(WOContext context) {
		super(context);
		ctrl = new CtrlRechercheDossier(this);
		qbe = new NSMutableDictionary<String, Object>();
		// qbe.takeValueForKey(new BigDecimal(session().anneeScolaire().fannDebut()), "annee");
	}

	public boolean isBusy() {
		return isDossierBusy();
	}

	public String styleForTd() {
		if (ctrl.uneInscription() != null && ctrl.uneInscription().isInscriptionPrincipale() == false) {
			return "color:rgb(100,100,100)";
		}
		return "";
	}

	public boolean isAfficherDetailEtudiant() {
		return ctrl.leEtudiant() != null;
	}

	public boolean isAfficherDossiersEtudiant() {
		return (isAfficherDetailEtudiant() && ctrl.leDossier() == null);
	}

	public boolean isAfficherDossierEtudiant() {
		return (isAfficherDetailEtudiant() && ctrl.leDossier() != null);
	}

	public boolean isPlusieursEtablissements() {
		return ctrl.listeEtablissements().count() > 1;
	}

	public void retourListeEtudiants() {
		ctrl.setLeDossier(null);
		ctrl.setLeEtudiant(null);
	}

	public void retourListeDossiers() {
		if (session().isBusy()) {
			mySession().addSimpleErrorMessage("Non", "Edition/modification en cours...");
			return;
		}
		ctrl.setLeDossier(null);
		if (ctrl.dossiers() != null && ctrl.dossiers().count() == 1) {
			ctrl.setLeEtudiant(null);
		}
	}

	public NSMutableDictionary<String, Object> qbe() {
		return qbe;
	}

	public void setQbe(NSMutableDictionary<String, Object> qbe) {
		this.qbe = qbe;
	}

	public void addToQbe(Object object, String key) {
		qbe.takeValueForKey(object, key);
	}

	public WOActionResults afficherCadre() {
		return null;
	}

	public WOActionResults submit() {
		return null;
	}

	public String nomMarital() {
		return (!ctrl.unEtudiant().individu().nomAffichage().equals(ctrl.unEtudiant().individu().nomPatronymiqueAffichage()) ? ctrl.unEtudiant()
				.individu().nomAffichage() : null);
	}

	public boolean isDossierBusy() {
		return isDossierBusy;
	}

	public void setIsDossierBusy(boolean isDossierBusy) {
		this.isDossierBusy = isDossierBusy;
	}
}