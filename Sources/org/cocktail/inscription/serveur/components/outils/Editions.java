/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.inscription.serveur.components.outils;

import java.util.GregorianCalendar;

import org.cocktail.inscription.serveur.components.MyComponent;
import org.cocktail.scolarix.serveur.impression.PrintFactory;
import org.cocktail.scolarix.serveur.metier.eos.EOScolFormationHabilitation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.qualifiers.ERXKeyValueQualifier;

public class Editions extends MyComponent {

	private NSTimestamp boursiersDateDeb, boursiersDateFin;
	private boolean isBoursiersParFiliereOpened = false, isBoursiersParDomaineOpened = false, isBoursiersGlobaleOpened = false;
	private boolean isAdmListeOpened = false, isAdmListeResultatOpened = false;
	private String scolCycle, scolCycleResultat;
	private EOScolFormationHabilitation uneFormation, laFormation;

	public Editions(WOContext context) {
		super(context);
	}

	public Integer persIdUtilisateur() {
		return new Integer(session.connectedUserInfo().persId().intValue());
	}

	public NSArray<EOScolFormationHabilitation> listeFormations() {
		// ERXSortOrderings sorts = ERXS.ascs(EOScolFormationHabilitation.TRI_KEY);
		NSMutableArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>(3);
		sortOrderings.addObject(EOSortOrdering.sortOrderingWithKey(EOScolFormationHabilitation.TRI_KEY, EOSortOrdering.CompareAscending));
		sortOrderings.addObject(EOSortOrdering.sortOrderingWithKey(EOScolFormationHabilitation.LIBELLE_ABREGE_KEY, EOSortOrdering.CompareAscending));
		sortOrderings.addObject(EOSortOrdering.sortOrderingWithKey(EOScolFormationHabilitation.FHAB_NIVEAU_KEY, EOSortOrdering.CompareAscending));
		ERXKeyValueQualifier qual1 = ERXQ.equals(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, session.anneeScolaire());
		ERXKeyValueQualifier qual2 = ERXQ.equals(EOScolFormationHabilitation.FHAB_OUVERT_KEY, EOScolFormationHabilitation.HABILITATION_OUVERTE);
		NSArray<EOScolFormationHabilitation> formations = EOScolFormationHabilitation.fetchAll(edc(), ERXQ.and(qual1, qual2));
		formations = ERXS.sorted(formations, sortOrderings);
		return formations;
	}

	private void closeAll() {
		setIsBoursiersParFiliereOpened(false);
		setIsBoursiersGlobaleOpened(false);
		setIsBoursiersParDomaineOpened(false);
		setIsAdmListeResultatOpened(false);
		setIsAdmListeOpened(false);
	}

	public void boursiersParFiliere() {
		closeAll();
		setIsBoursiersParFiliereOpened(true);
	}

	public void boursiersParDomaine() {
		closeAll();
		setIsBoursiersParDomaineOpened(true);
	}

	public void boursiersGlobale() {
		closeAll();
		setIsBoursiersGlobaleOpened(true);
	}

	public void admListe() {
		closeAll();
		setIsAdmListeOpened(true);
	}

	public void admListeResultat() {
		closeAll();
		setIsAdmListeResultatOpened(true);
	}

	public WOActionResults printBoursiersParFiliere() {
		try {
			Integer anneeScol = session.anneeScolaire().fannDebut();
			String cRne = session.etablissement().cRne();

			NSData data = PrintFactory.printBoursiersParFiliere(session, cRne, anneeScol);
			String fileName = "boursiers_par_filiere_" + anneeScol + "_" + cRne + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer la liste des boursiers par filière " + fileName);
			}
			return PrintFactory.afficherPdf(data, fileName);
		}
		catch (Throwable e) {
			e.printStackTrace();
			session.addSimpleErrorMessage("Erreur", e.getMessage());
			return null;
		}
	}

	public WOActionResults printBoursiersParDomaine() {
		try {
			Integer anneeScol = session.anneeScolaire().fannDebut();
			String cRne = session.etablissement().cRne();

			NSData data = PrintFactory.printBoursiersParDomaine(session, cRne, anneeScol, boursiersDateDeb(), boursiersDateFin());
			String fileName = "boursiers_par_domaine_" + anneeScol + "_" + cRne + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer la liste des boursiers par domaine " + fileName);
			}
			return PrintFactory.afficherPdf(data, fileName);
		}
		catch (Throwable e) {
			e.printStackTrace();
			session.addSimpleErrorMessage("Erreur", e.getMessage());
			return null;
		}
	}

	public WOActionResults printBoursiersGlobal() {
		try {
			Integer anneeScol = session.anneeScolaire().fannDebut();
			String cRne = session.etablissement().cRne();

			NSData data = PrintFactory.printBoursiersGlobal(session, cRne, anneeScol, boursiersDateDeb(), boursiersDateFin());
			String fileName = "boursiers_globale_" + anneeScol + "_" + cRne + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer la liste des boursiers globale " + fileName);
			}
			return PrintFactory.afficherPdf(data, fileName);
		}
		catch (Throwable e) {
			e.printStackTrace();
			session.addSimpleErrorMessage("Erreur", e.getMessage());
			return null;
		}
	}

	public WOActionResults printAdmListe() {
		try {
			if (laFormation() == null) {
				return null;
			}
			String scolCycle = laFormation().toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().fdipAbreviation()
					+ laFormation().fhabNiveau();
			Integer anneeScol = session.anneeScolaire().fannDebut();
			String cRne = session.etablissement().cRne();

			NSData data = PrintFactory.printAdmListe(session, cRne, anneeScol, scolCycle);
			String fileName = "scola_admlist_" + anneeScol + "_" + cRne + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer la liste des admissions " + fileName);
			}
			return PrintFactory.afficherPdf(data, fileName);
		}
		catch (Throwable e) {
			e.printStackTrace();
			session.addSimpleErrorMessage("Erreur", e.getMessage());
			return null;
		}
	}

	public WOActionResults printAdmListeResultat() {
		try {
			if (laFormation() == null) {
				return null;
			}
			String scolCycle = laFormation().toFwkScolarite_ScolFormationSpecialisation().toFwkScolarite_ScolFormationDiplome().fdipAbreviation()
					+ laFormation().fhabNiveau();
			Integer anneeScol = session.anneeScolaire().fannDebut();
			String cRne = session.etablissement().cRne();

			NSData data = PrintFactory.printAdmListeResultat(session, cRne, anneeScol, scolCycle);
			String fileName = "scola_admresult_admis_" + anneeScol + "_" + cRne + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer la liste des admissions résultat " + fileName);
			}
			return PrintFactory.afficherPdf(data, fileName);
		}
		catch (Throwable e) {
			e.printStackTrace();
			session.addSimpleErrorMessage("Erreur", e.getMessage());
			return null;
		}
	}

	public NSTimestamp boursiersDateDeb() {
		if (boursiersDateDeb == null) {
			int anneeScolDebut = session.anneeScolaire().fannDebut().intValue();
			if (anneeScolDebut != session.anneeScolaire().fannFin().intValue()) {
				boursiersDateDeb = new NSTimestamp(new GregorianCalendar(anneeScolDebut, 5, 1).getTime());
			}
			else {
				boursiersDateDeb = new NSTimestamp(new GregorianCalendar(anneeScolDebut - 1, 11, 1).getTime());
			}
		}
		return boursiersDateDeb;
	}

	public void setBoursiersDateDeb(NSTimestamp boursiersDateDeb) {
		this.boursiersDateDeb = boursiersDateDeb;
	}

	public NSTimestamp boursiersDateFin() {
		if (boursiersDateFin == null) {
			int anneeScolFin = session.anneeScolaire().fannFin().intValue();
			if (anneeScolFin != session.anneeScolaire().fannDebut().intValue()) {
				boursiersDateFin = new NSTimestamp(new GregorianCalendar(anneeScolFin, 4, 31).getTime());
			}
			else {
				boursiersDateFin = new NSTimestamp(new GregorianCalendar(anneeScolFin, 10, 30).getTime());
			}
		}
		return boursiersDateFin;
	}

	public void setBoursiersDateFin(NSTimestamp boursiersDateFin) {
		this.boursiersDateFin = boursiersDateFin;
	}

	public boolean isBoursiersParDomaineOpened() {
		return isBoursiersParDomaineOpened;
	}

	public void setIsBoursiersParDomaineOpened(boolean isBoursiersParDomaineOpened) {
		this.isBoursiersParDomaineOpened = isBoursiersParDomaineOpened;
	}

	public boolean isBoursiersGlobaleOpened() {
		return isBoursiersGlobaleOpened;
	}

	public void setIsBoursiersGlobaleOpened(boolean isBoursiersGlobaleOpened) {
		this.isBoursiersGlobaleOpened = isBoursiersGlobaleOpened;
	}

	public String scolCycle() {
		return scolCycle;
	}

	public void setScolCycle(String scolCycle) {
		this.scolCycle = scolCycle;
	}

	public boolean isAdmListeOpened() {
		return isAdmListeOpened;
	}

	public void setIsAdmListeOpened(boolean isAdmListeOpened) {
		this.isAdmListeOpened = isAdmListeOpened;
	}

	public boolean isAdmListeResultatOpened() {
		return isAdmListeResultatOpened;
	}

	public void setIsAdmListeResultatOpened(boolean isAdmListeResultatOpened) {
		this.isAdmListeResultatOpened = isAdmListeResultatOpened;
	}

	public String scolCycleResultat() {
		return scolCycleResultat;
	}

	public void setScolCycleResultat(String scolCycleResultat) {
		this.scolCycleResultat = scolCycleResultat;
	}

	public boolean isBoursiersParFiliereOpened() {
		return isBoursiersParFiliereOpened;
	}

	public void setIsBoursiersParFiliereOpened(boolean isBoursiersParFiliereOpened) {
		this.isBoursiersParFiliereOpened = isBoursiersParFiliereOpened;
	}

	public EOScolFormationHabilitation uneFormation() {
		return uneFormation;
	}

	public void setUneFormation(EOScolFormationHabilitation uneFormation) {
		this.uneFormation = uneFormation;
	}

	public EOScolFormationHabilitation laFormation() {
		return laFormation;
	}

	public void setLaFormation(EOScolFormationHabilitation laFormation) {
		this.laFormation = laFormation;
	}

}