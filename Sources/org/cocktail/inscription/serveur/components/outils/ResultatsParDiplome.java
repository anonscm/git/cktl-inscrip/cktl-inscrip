/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.inscription.serveur.components.outils;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.inscription.serveur.components.MyComponent;
import org.cocktail.scolaritefwk.serveur.interfaces.IScolFormationGrade;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationDiplome;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationFiliere;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationGrade;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolInscriptionTitre;
import org.cocktail.scolarix.serveur.finder.FinderScolFormationHabilitation;
import org.cocktail.scolarix.serveur.impression.PrintFactory;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOGarnucheParametres;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;
import org.cocktail.scolarix.serveur.metier.eos.EOInscDipl;
import org.cocktail.scolarix.serveur.metier.eos.EOMention;
import org.cocktail.scolarix.serveur.metier.eos.EOResultat;
import org.cocktail.scolarix.serveur.metier.eos.EOScolFormationHabilitation;
import org.cocktail.scolarix.serveur.metier.eos.EOVComposanteScolarite;
import org.cocktail.scolarix.serveur.metier.eos.EOVEtablissementScolarite;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.qualifiers.ERXKeyValueQualifier;

public class ResultatsParDiplome extends MyComponent {

	private WODisplayGroup dgDiplomes, dgEtudiants;
	private NSArray<EOScolFormationHabilitation> formationsEnvisageables = null;
	private EOScolFormationHabilitation unDiplome;
	private EOInscDipl uneInscription;

	private NSArray<EOVEtablissementScolarite> etablissements;
	private EOVEtablissementScolarite etablissementSelected;
	private boolean isTypeLmdSelected = true, isTypeClassiqueSelected = false;
	private IScolFormationGrade unGrade;
	private IScolFormationGrade grade;
	private String libelle;
	private NSArray<IScolFormationGrade> gradesGrades = null;
	private NSArray<IScolFormationGrade> gradesFilieres = null;

	private NSArray<EOResultat> resultats;
	private NSArray<EOMention> mentions;
	private EOResultat unResultat;
	private EOMention uneMention;

	private boolean isEditing = false, afficherNomenclatureResultats = false, afficherNomenclatureMentions = false;

	public ResultatsParDiplome(WOContext context) {
		super(context);
		reset();
	}

	public void reset() {
		setFormationsEnvisageables(null);
		dgDiplomesUpdateData();
	}

	public boolean isBusy() {
		return isEditing();
	}

	public WODisplayGroup dgDiplomes() {
		if (dgDiplomes == null) {
			dgDiplomes = new WODisplayGroup();
			dgDiplomes.setSelectsFirstObjectAfterFetch(false);
			NSMutableArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>(3);
			sortOrderings.addObject(ERXS.asc(EOScolFormationHabilitation.GRADE_LIBELLE_KEY));
			sortOrderings.addObject(ERXS.asc(EOScolFormationHabilitation.LIBELLE_ABREGE_KEY));
			sortOrderings.addObject(ERXS.asc(EOScolFormationHabilitation.FHAB_NIVEAU_KEY));
			dgDiplomes.setSortOrderings(sortOrderings);
		}
		return dgDiplomes;
	}

	public void dgDiplomesUpdateData() {
		dgDiplomes().setObjectArray(formationsEnvisageablesFiltered());
		if (dgDiplomes().displayedObjects() != null && dgDiplomes().displayedObjects().count() > 0) {
			dgDiplomes().setSelectionIndexes(new NSArray<Integer>(Integer.valueOf(0)));
			onSelectDiplome();
		}
	}

	public WOActionResults onSelectDiplome() {
		dgEtudiantsUpdateData();
		return null;
	}

	public WODisplayGroup dgEtudiants() {
		if (dgEtudiants == null) {
			dgEtudiants = new WODisplayGroup();
			dgEtudiants.setSelectsFirstObjectAfterFetch(false);
			dgEtudiants.setSortOrderings(ERXS.ascInsensitives(EOInscDipl.TO_HISTORIQUE_KEY + "." + EOHistorique.TO_ETUDIANT_KEY + "."
					+ EOEtudiant.TO_FWKPERS__INDIVIDU_KEY + "." + EOIndividu.NOM_AFFICHAGE_KEY));
		}
		return dgEtudiants;
	}

	public void dgEtudiantsUpdateData() {
		if (dgDiplomes().selectedObject() != null) {
			EOScolFormationHabilitation diplome = (EOScolFormationHabilitation) dgDiplomes().selectedObject();
			ERXKeyValueQualifier q1 = ERXQ.equals(EOInscDipl.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY,
					diplome.toFwkScolarite_ScolFormationSpecialisation());
			ERXKeyValueQualifier q2 = ERXQ.equals(EOInscDipl.TO_HISTORIQUE_KEY + "." + EOHistorique.HIST_ANNEE_SCOL_KEY, diplome
					.toFwkScolarite_ScolFormationAnnee().fannDebut());
			ERXKeyValueQualifier q3 = ERXQ.equals(EOInscDipl.IDIPL_ANNEE_SUIVIE_KEY, diplome.fhabNiveau());

			EOFetchSpecification fetchSpec = new EOFetchSpecification(EOInscDipl.ENTITY_NAME, ERXQ.and(q1, q2, q3), null);
			fetchSpec.setIsDeep(true);
			fetchSpec.setUsesDistinct(true);
			fetchSpec.setPrefetchingRelationshipKeyPaths(new NSArray<String>(new String[] { EOInscDipl.TO_HISTORIQUE_KEY + "."
					+ EOHistorique.TO_ETUDIANT_KEY + "." + EOEtudiant.TO_FWKPERS__INDIVIDU_KEY }));
			dgEtudiants().setObjectArray(edc().objectsWithFetchSpecification(fetchSpec));
		}
		else {
			dgEtudiants().setObjectArray(null);
		}
		if (dgEtudiants().displayedObjects() != null && dgEtudiants().displayedObjects().count() > 0) {
			dgEtudiants().setSelectionIndexes(new NSArray<Integer>(Integer.valueOf(0)));
			onSelectEtudiant();
		}
	}

	public WOActionResults printResultats() {
		try {
			EOScolFormationHabilitation diplome = (EOScolFormationHabilitation) dgDiplomes().selectedObject();
			if (diplome == null) {
				return null;
			}
			Integer anneeScol = diplome.toFwkScolarite_ScolFormationAnnee().fannDebut();
			String cRne = (etablissementSelected() != null ? etablissementSelected().cRne() : null);
			Integer dspeCode = diplome.toFwkScolarite_ScolFormationSpecialisation().fspnKey();
			String orderByKeys = "";
			for (EOSortOrdering sort : dgEtudiants().sortOrderings()) {
				if (orderByKeys.length() > 0) {
					orderByKeys += ", ";
				}
				orderByKeys += sort.key().substring(sort.key().lastIndexOf(".") + 1)
						+ (sort.selector().equals(EOSortOrdering.CompareAscending)
								|| sort.selector().equals(EOSortOrdering.CompareCaseInsensitiveAscending) ? " asc" : " desc");
			}
			String libelleDiplome = diplome.libelleDiplomeComplet() + " " + diplome.toFwkScolarite_ScolFormationAnnee().toString();

			NSData data = PrintFactory.printResultats(session, cRne, EOGarnucheParametres.getGarnucheUniversite(edc()),
					EOGarnucheParametres.getGarnucheVille(edc()), dspeCode, diplome.fhabNiveau(), anneeScol, orderByKeys, libelleDiplome);
			String fileName = "liste_resultats_" + anneeScol + "_" + cRne + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer la liste des résultats " + fileName);
			}
			return PrintFactory.afficherPdf(data, fileName);
		}
		catch (Throwable e) {
			e.printStackTrace();
			session.addSimpleErrorMessage("Erreur", e.getMessage());
			return null;
		}
	}

	public WOActionResults onSelectEtudiant() {
		return null;
	}

	public WOActionResults rechercherDiplomes() {
		dgDiplomesUpdateData();
		return null;
	}

	public void changeType() {
		setGrade(null);
		rechercherDiplomes();
	}

	public WOActionResults editer() {
		setIsEditing(true);
		return null;
	}

	public WOActionResults afficherNomenclature() {
		if (isAfficherNomenclatureResultats()) {
			setIsAfficherNomenclatureResultats(false);
			setIsAfficherNomenclatureMentions(true);
			return null;
		}
		if (isAfficherNomenclatureMentions()) {
			setIsAfficherNomenclatureMentions(false);
			return null;
		}
		setIsAfficherNomenclatureResultats(true);
		return null;
	}

	public boolean isAfficherNomenclature() {
		return isAfficherNomenclatureResultats() || isAfficherNomenclatureMentions();
	}

	public String titleImageNomenclature() {
		if (!isAfficherNomenclature()) {
			return "Afficher la nomenclature des résultats";
		}
		if (isAfficherNomenclatureResultats()) {
			return "Afficher la nomenclature des mentions";
		}
		if (isAfficherNomenclatureMentions()) {
			return "Cacher les nomenclatures";
		}
		return "";
	}

	public String classSpanTableViewEtudiants() {
		if (!isAfficherNomenclature()) {
			return "useMaxWidth";
		}
		return "";
	}

	public String styleSpanTableViewEtudiants() {
		if (isAfficherNomenclature()) {
			return "float:left;width:550px;";
		}
		return "float:left;";
	}

	public boolean isTitreDisabled() {
		if (isNotEditing()) {
			return true;
		}
		if (dgDiplomes().selectedObject() != null) {
			EOScolFormationHabilitation diplome = (EOScolFormationHabilitation) dgDiplomes().selectedObject();
			if (!diplome.hasTitre().booleanValue()) {
				return true;
			}
			EOScolInscriptionTitre insc = uneInscription().getInscriptionTitre();
			if (insc == null || insc.iftitEtat() == null || insc.iftitEtat().intValue() != 1) {
				return false;
			}
		}
		return true;
	}

	public WOActionResults annuler() {
		try {
			edc().revert();
			edc().saveChanges();
			session().addSimpleInfoMessage("OK", "Modifications annulées !");
			setIsEditing(false);
			dgEtudiants().displayedObjects().takeValueForKey(null, "isTitre");
		}
		catch (Exception e) {
			e.printStackTrace();
			session().addSimpleErrorMessage("Problème !", "Problème lors de l'annulation !!");
		}
		return null;
	}

	public WOActionResults enregistrer() {
		try {
			edc().saveChanges();
			session().addSimpleInfoMessage("OK", "Modifications enregistrées");
			setIsEditing(false);
		}
		catch (Exception e) {
			e.printStackTrace();
			session().addSimpleErrorMessage("Problème !", "Problème lors de l'enregistrement !!");
		}
		return null;
	}

	// public String resultatName() {
	// return "resultatName" + uneInscription().idiplNumero();
	// }
	//
	// public String resultatId() {
	// return "resultatId" + uneInscription().idiplNumero();
	// }

	public NSArray<EOResultat> resultatsFiltered() {
		if (resultats() != null && uneInscription() != null && uneInscription().resultatInputValue() != null
				&& uneInscription().resultatInputValue().trim().length() > 0) {
			String filtre = uneInscription().resultatInputValue().trim();
			if (filtre.length() == 1) {
				return ERXQ.filtered(resultats(), ERXQ.likeInsensitive(EOResultat.RES_CODE_KEY, filtre));
			}
			else {
				return ERXQ.filtered(resultats(), ERXQ.likeInsensitive(EOResultat.RES_LIBELLE_KEY, "*" + filtre + "*"));
			}
		}
		return resultats();
	}

	public NSArray<EOMention> mentionsFiltered() {
		if (mentions() != null && uneInscription() != null && uneInscription().mentionInputValue() != null
				&& uneInscription().mentionInputValue().trim().length() > 0) {
			String filtre = uneInscription().mentionInputValue().trim();
			if (filtre.length() == 1) {
				return ERXQ.filtered(mentions(), ERXQ.equals(EOMention.MENT_CODE_KEY, filtre));
			}
			else {
				return ERXQ.filtered(mentions(), ERXQ.likeInsensitive(EOMention.MENT_LIBELLE_KEY, "*" + filtre + "*"));
			}
		}
		return mentions();
	}

	private NSArray<EOScolFormationHabilitation> formationsEnvisageablesFiltered() {
		if (formationsEnvisageables() != null) {
			NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
			if (isTypeLmdSelected()) {
				quals.addObject(ERXQ.equals(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
						+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
						+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE_KEY + "." + EOScolFormationGrade.FGRA_VALIDITE_KEY, "O"));
				if (grade() != null) {
					quals.addObject(ERXQ.equals(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
							+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
							+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_GRADE_KEY, grade()));
				}
			}
			else {
				quals.addObject(ERXQ.equals(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
						+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
						+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_FILIERE_KEY + "." + EOScolFormationFiliere.FFIL_VALIDITE_KEY, "O"));
				if (grade() != null) {
					quals.addObject(ERXQ.equals(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
							+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
							+ EOScolFormationDiplome.TO_FWK_SCOLARITE__SCOL_FORMATION_FILIERE_KEY, grade()));
				}
			}
			if (libelle() != null) {
				EOKeyValueQualifier qual1 = ERXQ.likeInsensitive(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY
						+ "." + EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
						+ EOScolFormationDiplome.FDIP_LIBELLE_KEY, "*" + libelle() + "*");
				EOKeyValueQualifier qual2 = ERXQ.likeInsensitive(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY
						+ "." + EOScolFormationSpecialisation.FSPN_LIBELLE_KEY, "*" + libelle() + "*");
				quals.addObject(ERXQ.or(qual1, qual2));
			}
			if (etablissementSelected() != null) {
				EOKeyValueQualifier qual1 = ERXQ.equals(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
						+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
						+ EOScolFormationDiplome.TO_FWK_SCOLARIX_V_ETABLISSEMENT_SCOLARITE_KEY + "." + EOVEtablissementScolarite.TO_RNE_KEY,
						etablissementSelected().toRne());
				EOKeyValueQualifier qual2 = ERXQ.equals(EOScolFormationHabilitation.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
						+ EOScolFormationSpecialisation.TO_FWK_SCOLARITE__SCOL_FORMATION_DIPLOME_KEY + "."
						+ EOScolFormationDiplome.TO_FWK_SCOLARIX_V_COMPOSANTE_SCOLARITE_KEY + "." + EOVComposanteScolarite.TO_RNE_KEY,
						etablissementSelected().toRne());
				quals.addObject(ERXQ.or(qual1, qual2));
			}
			if (quals.count() > 0) {
				return ERXQ.filtered(formationsEnvisageables(), ERXQ.and(quals));
			}
		}
		return null;

	}

	private NSArray<EOScolFormationHabilitation> formationsEnvisageables() {
		if (formationsEnvisageables == null) {
			formationsEnvisageables = FinderScolFormationHabilitation.getScolFormationHabilitations(edc(), session().anneeScolaire().fannDebut(),
					null);
		}
		return formationsEnvisageables;
	}

	public void setFormationsEnvisageables(NSArray<EOScolFormationHabilitation> formationsEnvisageables) {
		this.formationsEnvisageables = formationsEnvisageables;
	}

	public NSArray<IScolFormationGrade> grades() {
		if (isTypeLmdSelected()) {
			if (gradesGrades == null) {
				gradesGrades = EOScolFormationGrade.fetchAll(edc());
			}
			return ERXS.sorted(gradesGrades, ERXS.asc(EOScolFormationGrade.TRI_KEY));
		}
		else {
			if (gradesFilieres == null) {
				gradesFilieres = EOScolFormationFiliere.fetchAll(edc(), ERXS.ascInsensitives(EOScolFormationFiliere.FFIL_ABREVIATION_KEY));
			}
			return gradesFilieres;
		}
	}

	public String noSelectionStringGrade() {
		if (isTypeLmdSelected()) {
			return "Tous";
		}
		return "Toutes";
	}

	public Integer persIdUtilisateur() {
		return new Integer(session.connectedUserInfo().persId().intValue());
	}

	public boolean isTypeLmdSelected() {
		return isTypeLmdSelected;
	}

	public void setIsTypeLmdSelected(boolean isTypeLmdSelected) {
		this.isTypeLmdSelected = isTypeLmdSelected;
	}

	public boolean isTypeClassiqueSelected() {
		return isTypeClassiqueSelected;
	}

	public void setIsTypeClassiqueSelected(boolean isTypeClassiqueSelected) {
		this.isTypeClassiqueSelected = isTypeClassiqueSelected;
	}

	public IScolFormationGrade unGrade() {
		return unGrade;
	}

	public void setUnGrade(IScolFormationGrade unGrade) {
		this.unGrade = unGrade;
	}

	public IScolFormationGrade grade() {
		return grade;
	}

	public void setGrade(IScolFormationGrade grade) {
		this.grade = grade;
	}

	public String libelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public NSArray<IScolFormationGrade> gradesGrades() {
		return gradesGrades;
	}

	public void setGradesGrades(NSArray<IScolFormationGrade> gradesGrades) {
		this.gradesGrades = gradesGrades;
	}

	public NSArray<IScolFormationGrade> gradesFilieres() {
		return gradesFilieres;
	}

	public void setGradesFilieres(NSArray<IScolFormationGrade> gradesFilieres) {
		this.gradesFilieres = gradesFilieres;
	}

	public EOScolFormationHabilitation unDiplome() {
		return unDiplome;
	}

	public void setUnDiplome(EOScolFormationHabilitation unDiplome) {
		this.unDiplome = unDiplome;
	}

	public EOInscDipl uneInscription() {
		return uneInscription;
	}

	public void setUneInscription(EOInscDipl uneInscription) {
		this.uneInscription = uneInscription;
	}

	public boolean isEditing() {
		return isEditing;
	}

	public boolean isNotEditing() {
		return !isEditing;
	}

	public void setIsEditing(boolean isEditing) {
		this.isEditing = isEditing;
	}

	public NSArray<EOResultat> resultats() {
		if (resultats == null) {
			resultats = EOResultat.fetchAll(edc(), ERXS.ascs(EOResultat.RES_CODE_KEY));
		}
		return resultats;
	}

	public void setResultats(NSArray<EOResultat> resultats) {
		this.resultats = resultats;
	}

	public NSArray<EOMention> mentions() {
		if (mentions == null) {
			mentions = EOMention.fetchAll(edc(), ERXS.ascs(EOMention.MENT_CODE_KEY));
		}
		return mentions;
	}

	public void setMentions(NSArray<EOMention> mentions) {
		this.mentions = mentions;
	}

	public EOResultat unResultat() {
		return unResultat;
	}

	public void setUnResultat(EOResultat unResultat) {
		this.unResultat = unResultat;
	}

	public EOMention uneMention() {
		return uneMention;
	}

	public void setUneMention(EOMention uneMention) {
		this.uneMention = uneMention;
	}

	public boolean isAfficherNomenclatureResultats() {
		return afficherNomenclatureResultats;
	}

	public void setIsAfficherNomenclatureResultats(boolean afficherNomenclatureResultats) {
		this.afficherNomenclatureResultats = afficherNomenclatureResultats;
	}

	public boolean isAfficherNomenclatureMentions() {
		return afficherNomenclatureMentions;
	}

	public void setIsAfficherNomenclatureMentions(boolean afficherNomenclatureMentions) {
		this.afficherNomenclatureMentions = afficherNomenclatureMentions;
	}

	public NSArray<EOVEtablissementScolarite> etablissements() {
		if (etablissements == null) {
			etablissements = EOVEtablissementScolarite.fetchAll(edc());
		}
		return etablissements;
	}

	public void setEtablissements(NSArray<EOVEtablissementScolarite> etablissements) {
		this.etablissements = etablissements;
	}

	public EOVEtablissementScolarite etablissementSelected() {
		if (etablissementSelected == null) {
			etablissementSelected = session().etablissement();
		}
		return etablissementSelected;
	}

	public void setEtablissementSelected(EOVEtablissementScolarite etablissementSelected) {
		this.etablissementSelected = etablissementSelected;
	}

}