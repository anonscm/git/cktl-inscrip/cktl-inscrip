/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.inscription.serveur.components.outils;

import org.cocktail.inscription.serveur.components.MyComponent;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiantRembourse;
import org.cocktail.scolarix.serveur.metier.eos.EOVBordereauDetail;
import org.cocktail.scolarix.serveur.metier.eos.EOVBordereauEntete;
import org.cocktail.scolarix.serveur.process.ProcessInsBordereauNew;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.qualifiers.ERXKeyValueQualifier;
import er.extensions.qualifiers.ERXOrQualifier;

public class Bordereaux extends MyComponent {

	private WODisplayGroup dgBordereaux = null, dgDetailBordereaux = null, dgRemboursements = null;
	private EOVBordereauEntete unBord = null;
	private EOVBordereauDetail unDetailBord = null;
	private EOEtudiantRembourse unRemb;
	private boolean isAjoutEnCours = false;

	public Bordereaux(WOContext context) {
		super(context);
		reset();
	}

	public void reset() {
		dgBordereauxUpdateData();
	}

	public boolean isBusy() {
		return isAjoutEnCours();
	}

	public WODisplayGroup dgBordereaux() {
		if (dgBordereaux == null) {
			dgBordereaux = new WODisplayGroup();
			dgBordereaux.setSelectsFirstObjectAfterFetch(true);
			dgBordereaux.setSortOrderings(ERXS.desc(EOVBordereauEntete.EREMB_ANNEE_KEY).then(ERXS.desc(EOVBordereauEntete.EREMB_NUM_BORDEREAU_KEY)));
		}
		return dgBordereaux;
	}

	public void dgBordereauxUpdateData() {
		ERXOrQualifier q1 = ERXQ.inObjects(EOVBordereauEntete.EREMB_ANNEE_KEY, session().anneeScolaire().fannDebut(), session().anneeScolaire()
				.fannFin());
		ERXKeyValueQualifier q2 = ERXQ.equals(EOVBordereauEntete.ETAB_CODE_KEY, session().etablissement().cRne());
		dgBordereaux().setObjectArray(EOVBordereauEntete.fetchAll(edc(), ERXQ.and(q1, q2)));
		if (dgBordereaux().displayedObjects() != null && dgBordereaux().displayedObjects().count() > 0) {
			dgBordereaux().setSelectionIndexes(new NSArray<Integer>(Integer.valueOf(0)));
			onSelectBordereau();
		}
	}

	public WODisplayGroup dgDetailBordereaux() {
		if (dgDetailBordereaux == null) {
			dgDetailBordereaux = new WODisplayGroup();
			dgDetailBordereaux.setSelectsFirstObjectAfterFetch(false);
			dgDetailBordereaux.setSortOrderings(ERXS.asc(EOVBordereauDetail.NOM_KEY).then(ERXS.asc(EOVBordereauDetail.PRENOM_KEY)));
		}
		return dgDetailBordereaux;
	}

	public WODisplayGroup dgRemboursements() {
		if (dgRemboursements == null) {
			dgRemboursements = new WODisplayGroup();
			dgRemboursements.setSelectsFirstObjectAfterFetch(false);
		}
		return dgRemboursements;
	}

	public void dgRemboursementsUpdateData() {
		dgRemboursements().setObjectArray(
				EOEtudiantRembourse.fetchAll(
						edc(),
						ERXQ.isNull(EOEtudiantRembourse.EREMB_NUM_BORDEREAU_KEY).and(
								ERXQ.equals(EOEtudiantRembourse.TO_V_ETABLISSEMENT_SCOLARITE_KEY, session().etablissement()))));
	}

	public WOActionResults onSelectBordereau() {
		if (dgBordereaux().selectedObject() != null) {
			dgDetailBordereaux().setObjectArray(((EOVBordereauEntete) dgBordereaux().selectedObject()).toVBordereauDetails());
		}
		else {
			dgDetailBordereaux().setObjectArray(null);
		}
		return null;
	}

	public WOActionResults ajouter() {
		dgRemboursementsUpdateData();
		if (dgRemboursements().displayedObjects().count() > 0) {
			setAjoutEnCours(true);
		}
		else {
			session().addSimpleErrorMessage("Non", "Aucun remboursement en attente de bordereau...");
		}
		return null;
	}

	public WOActionResults regenerer() {
		try {
			if (dgBordereaux().selectedObject() != null) {
				// TODO
				// ProcessDelRemboursementPlus.enregistrer(cktlSession().dataBus(), edc(), (EOEtudiantRembourse)
				// dgRemboursements().selectedObject());
				dgBordereauxUpdateData();
			}
		}
		catch (ScolarixFwkException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
			return null;
		}
		catch (Exception e) {
			mySession().addSimpleErrorMessage("Erreur", e);
			return null;
		}
		return null;
	}

	public WOActionResults validerCreationBordereau() {
		try {
			ProcessInsBordereauNew.enregistrer(cktlSession().dataBus(), edc(), dgRemboursements().selectedObjects(), persIdUtilisateur());
		}
		catch (ScolarixFwkException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
			return null;
		}
		catch (Exception e) {
			mySession().addSimpleErrorMessage("Erreur", e);
			return null;
		}
		setAjoutEnCours(false);
		dgBordereauxUpdateData();
		session().addSimpleInfoMessage("OK", "Bordereau enregistré.");
		return null;
	}

	public WOActionResults annulerCreationBordereau() {
		edc().revert();
		setAjoutEnCours(false);
		session().addSimpleInfoMessage("Annulé", "Création de bordereau annulée.");
		return null;
	}

	public Integer persIdUtilisateur() {
		return new Integer(session.connectedUserInfo().persId().intValue());
	}

	public boolean isAjoutEnCours() {
		return isAjoutEnCours;
	}

	public void setAjoutEnCours(boolean isAjoutEnCours) {
		this.isAjoutEnCours = isAjoutEnCours;
	}

	public EOVBordereauEntete unBord() {
		return unBord;
	}

	public void setUnBord(EOVBordereauEntete unBord) {
		this.unBord = unBord;
	}

	public EOVBordereauDetail unDetailBord() {
		return unDetailBord;
	}

	public void setUnDetailBord(EOVBordereauDetail unDetailBord) {
		this.unDetailBord = unDetailBord;
	}

	public EOEtudiantRembourse unRemb() {
		return unRemb;
	}

	public void setUnRemb(EOEtudiantRembourse unRemb) {
		this.unRemb = unRemb;
	}

}