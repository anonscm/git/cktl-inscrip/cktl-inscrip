/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.inscription.serveur.components.outils;

import org.cocktail.inscription.serveur.components.MyComponent;
import org.cocktail.scolarix.serveur.exception.ScolarixFwkException;
import org.cocktail.scolarix.serveur.impression.PrintFactory;
import org.cocktail.scolarix.serveur.metier.eos.EORdvPlanningInfo;
import org.cocktail.scolarix.serveur.metier.eos.EOVRdvCandidats;
import org.cocktail.scolarix.serveur.metier.eos.EOVRdvHoraires;
import org.cocktail.scolarix.serveur.metier.eos.EOVRdvJournees;
import org.cocktail.scolarix.serveur.process.ProcessDelRdvCandidat;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class Rdv extends MyComponent {

	private NSArray<EORdvPlanningInfo> rdvPlanningInfos = null;
	private EORdvPlanningInfo leRdvPlanningInfo = null;
	private WODisplayGroup dgRdvJournees = null, dgRdvHoraires = null, dgRdvCandidats = null;
	private EOVRdvJournees unRdvJournees = null;
	private EOVRdvHoraires unRdvHoraires = null;
	private EOVRdvCandidats unRdvCandidats = null;

	public Rdv(WOContext context) {
		super(context);
	}

	public void reset() {
		rdvPlanningInfos = null;
	}

	public NSArray<EORdvPlanningInfo> rdvPlanningInfos() {
		if (rdvPlanningInfos == null) {
			rdvPlanningInfos = EORdvPlanningInfo.fetchAll(edc(), ERXS.ascs(EORdvPlanningInfo.NUM_OCC_PLANNING_KEY));
			if (rdvPlanningInfos != null && rdvPlanningInfos.count() > 0) {
				setLeRdvPlanningInfo(rdvPlanningInfos.objectAtIndex(0));
				onSelectRdvPlanningInfo();
			}
		}
		return rdvPlanningInfos;
	}

	public WOActionResults onSelectRdvPlanningInfo() {
		dgRdvJourneesUpdateData();
		return null;
	}

	public WODisplayGroup dgRdvJournees() {
		if (dgRdvJournees == null) {
			dgRdvJournees = new WODisplayGroup();
			dgRdvJournees.setSelectsFirstObjectAfterFetch(false);
			dgRdvJournees.setSortOrderings(ERXS.ascs(EOVRdvJournees.DATE_COMPLETE_KEY));
		}
		return dgRdvJournees;
	}

	public void dgRdvJourneesUpdateData() {
		dgRdvJournees().setObjectArray(
				EOVRdvJournees.fetchAll(edc(), ERXQ.equals(EOVRdvJournees.NUM_OCC_PLANNING_KEY, leRdvPlanningInfo().numOccPlanning())));
		if (dgRdvJournees().displayedObjects() != null && dgRdvJournees().displayedObjects().count() > 0) {
			dgRdvJournees().setSelectionIndexes(new NSArray<Integer>(Integer.valueOf(0)));
			onSelectRdvJournees();
		}
	}

	public WOActionResults onSelectRdvJournees() {
		dgRdvHorairesUpdateData();
		return null;
	}

	public String totalRdvPoses() {
		if (dgRdvJournees().selectedObject() != null) {
			return ((EOVRdvJournees) dgRdvJournees().selectedObject()).toVRdvHoraireses().valueForKey("@sum." + EOVRdvHoraires.NB_RDV_POSES_KEY)
					.toString();
		}
		return null;
	}

	public String totalRdvVenus() {
		if (dgRdvJournees().selectedObject() != null) {
			return ((EOVRdvJournees) dgRdvJournees().selectedObject()).toVRdvHoraireses().valueForKey("@sum." + EOVRdvHoraires.NB_RDV_VENUS_KEY)
					.toString();
		}
		return null;
	}

	public WODisplayGroup dgRdvHoraires() {
		if (dgRdvHoraires == null) {
			dgRdvHoraires = new WODisplayGroup();
			dgRdvHoraires.setSelectsFirstObjectAfterFetch(false);
			dgRdvHoraires.setSortOrderings(ERXS.ascs(EOVRdvHoraires.DEB_AFFICHAGE_KEY));
		}
		return dgRdvHoraires;
	}

	public void dgRdvHorairesUpdateData() {
		if (dgRdvJournees().selectedObject() != null) {
			dgRdvHoraires().setObjectArray(((EOVRdvJournees) dgRdvJournees().selectedObject()).toVRdvHoraireses());
		}
		else {
			dgRdvHoraires().setObjectArray(null);
		}
		if (dgRdvHoraires().displayedObjects() != null && dgRdvHoraires().displayedObjects().count() > 0) {
			dgRdvHoraires().setSelectionIndexes(new NSArray<Integer>(Integer.valueOf(0)));
			onSelectRdvHoraires();
		}
	}

	public WOActionResults onSelectRdvHoraires() {
		dgRdvCandidatsUpdateData();
		return null;
	}

	public String styleForTDTableViewRdvHoraires() {
		if (unRdvHoraires() != null && unRdvHoraires().nbRdvRest().intValue() == 0) {
			return "color:#FF0000";
		}
		return "";
	}

	public WODisplayGroup dgRdvCandidats() {
		if (dgRdvCandidats == null) {
			dgRdvCandidats = new WODisplayGroup();
			dgRdvCandidats.setSelectsFirstObjectAfterFetch(false);
			dgRdvCandidats.setSortOrderings(ERXS.ascs(EOVRdvCandidats.ETUD_NUMERO_KEY));
		}
		return dgRdvCandidats;
	}

	public void dgRdvCandidatsUpdateData() {
		if (dgRdvHoraires().selectedObject() != null) {
			dgRdvCandidats().setObjectArray(((EOVRdvHoraires) dgRdvHoraires().selectedObject()).toVRdvCandidatses());
		}
		else {
			dgRdvCandidats().setObjectArray(null);
		}
	}

	public String temoinRdv() {
		return (unRdvCandidats().temRdv() != null && unRdvCandidats().temRdv().equals("I") ? "*" : "");
	}

	public WOActionResults supprimer() {
		try {
			if (dgRdvCandidats().selectedObject() != null) {
				ProcessDelRdvCandidat.enregistrer(cktlSession().dataBus(), edc(), (EOVRdvCandidats) dgRdvCandidats().selectedObject());
				dgRdvHorairesUpdateData();
				mySession().addSimpleInfoMessage("OK", "RDV supprimé");
			}
		}
		catch (ScolarixFwkException e) {
			mySession().addSimpleErrorMessage("Erreur", e.getMessageFormatte());
			return null;
		}
		catch (Exception e) {
			mySession().addSimpleErrorMessage("Erreur", e);
			return null;
		}
		return null;
	}

	public WOActionResults printRdvListe() {
		try {
			if (dgRdvJournees().selectedObject() == null) {
				return null;
			}
			String cRne = session.etablissement().cRne();
			Integer numOccPlanning = ((EOVRdvJournees) dgRdvJournees().selectedObject()).numOccPlanning();
			String convocDateString = ((EOVRdvJournees) dgRdvJournees().selectedObject()).datePer();

			NSData data = PrintFactory.printRdvListe(session, cRne, numOccPlanning, convocDateString);
			String fileName = "scola_rdv_liste_" + cRne + ".pdf";
			if (data == null) {
				throw new Exception("Impossible d'imprimer la liste des RDV " + fileName);
			}
			return PrintFactory.afficherPdf(data, fileName);
		}
		catch (Throwable e) {
			e.printStackTrace();
			session.addSimpleErrorMessage("Erreur", e.getMessage());
			return null;
		}
	}

	public Integer persIdUtilisateur() {
		return new Integer(session.connectedUserInfo().persId().intValue());
	}

	public EORdvPlanningInfo leRdvPlanningInfo() {
		return leRdvPlanningInfo;
	}

	public void setLeRdvPlanningInfo(EORdvPlanningInfo leRdvPlanningInfo) {
		this.leRdvPlanningInfo = leRdvPlanningInfo;
	}

	public EOVRdvJournees unRdvJournees() {
		return unRdvJournees;
	}

	public void setUnRdvJournees(EOVRdvJournees unRdvJournees) {
		this.unRdvJournees = unRdvJournees;
	}

	public EOVRdvHoraires unRdvHoraires() {
		return unRdvHoraires;
	}

	public void setUnRdvHoraires(EOVRdvHoraires unRdvHoraires) {
		this.unRdvHoraires = unRdvHoraires;
	}

	public EOVRdvCandidats unRdvCandidats() {
		return unRdvCandidats;
	}

	public void setUnRdvCandidats(EOVRdvCandidats unRdvCandidats) {
		this.unRdvCandidats = unRdvCandidats;
	}

}