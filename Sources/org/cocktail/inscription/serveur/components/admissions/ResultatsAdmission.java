/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.inscription.serveur.components.admissions;

import org.cocktail.inscription.serveur.components.MyComponent;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationAdmission;
import org.cocktail.scolaritefwk.serveur.metier.eos.EOScolFormationSpecialisation;
import org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum;
import org.cocktail.scolarix.serveur.metier.eos.EOResultat;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;
import er.extensions.qualifiers.ERXKeyValueQualifier;

public class ResultatsAdmission extends MyComponent {

	private WODisplayGroup dgEtudiants;
	private EOCandidatGrhum uneInscription;

	private NSArray<EOResultat> resultats;
	private EOResultat unResultat;

	public EOScolFormationAdmission uneFormationAdmission, selectedFormationAdmission;
	private boolean isEditing = false;

	public ResultatsAdmission(WOContext context) {
		super(context);
		reset();
	}

	public void reset() {
		selectedFormationAdmission = null;
		onSelectFormationAdmission();
	}

	public boolean isBusy() {
		return isEditing();
	}

	public WOActionResults onSelectFormationAdmission() {
		dgEtudiantsUpdateData();
		return null;
	}

	public NSArray<EOScolFormationAdmission> listeFormationAdmissions() {
		EOQualifier q = ERXQ.equals(EOScolFormationAdmission.TO_FWK_SCOLARITE__SCOL_FORMATION_ANNEE_KEY, session().anneeScolaire()).and(
				ERXQ.equals(EOScolFormationAdmission.FADM_ADMISSION_KEY, "O"));
		ERXSortOrderings sorts = ERXS.ascs(EOScolFormationAdmission.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY + "."
				+ EOScolFormationSpecialisation.FSPN_LIBELLE_KEY, EOScolFormationAdmission.FADM_NIVEAU_KEY);
		return EOScolFormationAdmission.fetchAll(edc(), q, sorts);
	}

	public WODisplayGroup dgEtudiants() {
		if (dgEtudiants == null) {
			dgEtudiants = new WODisplayGroup();
			dgEtudiants.setSelectsFirstObjectAfterFetch(false);
			dgEtudiants.setSortOrderings(ERXS.ascInsensitives(EOCandidatGrhum.CAND_NOM_KEY, EOCandidatGrhum.CAND_PRENOM_KEY));
		}
		return dgEtudiants;
	}

	public void dgEtudiantsUpdateData() {
		if (selectedFormationAdmission != null) {
			ERXKeyValueQualifier q1 = ERXQ.equals(EOCandidatGrhum.TO_FWK_SCOLARITE__SCOL_FORMATION_SPECIALISATION_KEY,
					selectedFormationAdmission.toFwkScolarite_ScolFormationSpecialisation());
			ERXKeyValueQualifier q2 = ERXQ.equals(EOCandidatGrhum.CAND_ANNEE_SCOL_KEY, selectedFormationAdmission.toFwkScolarite_ScolFormationAnnee()
					.fannDebut());
			ERXKeyValueQualifier q3 = ERXQ.equals(EOCandidatGrhum.CAND_ANNEE_SUIVIE_KEY, selectedFormationAdmission.fadmNiveau());

			dgEtudiants().setObjectArray(EOCandidatGrhum.fetchAll(edc(), ERXQ.and(q1, q2, q3)));
		}
		else {
			dgEtudiants().setObjectArray(null);
		}
		if (dgEtudiants().displayedObjects() != null && dgEtudiants().displayedObjects().count() > 0) {
			dgEtudiants().setSelectionIndexes(new NSArray<Integer>(Integer.valueOf(0)));
			onSelectEtudiant();
		}
	}

	public WOActionResults onSelectEtudiant() {
		return null;
	}

	public WOActionResults editer() {
		setIsEditing(true);
		return null;
	}

	public WOActionResults annuler() {
		try {
			edc().revert();
			edc().saveChanges();
			session().addSimpleInfoMessage("OK", "Modifications annulées !");
			setIsEditing(false);
		}
		catch (Exception e) {
			e.printStackTrace();
			session().addSimpleErrorMessage("Problème !", "Problème lors de l'annulation !!");
		}
		return null;
	}

	public WOActionResults enregistrer() {
		try {
			edc().saveChanges();
			session().addSimpleInfoMessage("OK", "Modifications enregistrées");
			setIsEditing(false);
		}
		catch (Exception e) {
			e.printStackTrace();
			session().addSimpleErrorMessage("Problème !", "Problème lors de l'enregistrement !!");
		}
		return null;
	}

	public boolean resultatVide() {
		return uneInscription().toResultat() == null;
	}

	public void setResultatVide(boolean b) {
		if (b) {
			uneInscription().setToResultatRelationship(null);
		}
	}

	public String radioButtonResultatId() {
		return radioButtonResultatName() + "_" + unResultat().resCode() + "_Id";
	}

	public String radioButtonResultatName() {
		return "RBResultatAdmission_" + uneInscription().primaryKey() + "_Name";
	}

	public String onCompleteRBResultat() {
		return "function (oC) {Form.Element.activate('" + classementCellId() + "');}";
	}

	public String classementCellId() {
		return "ClassementCell_" + uneInscription().primaryKey() + "_Id";
	}

	public EOCandidatGrhum uneInscription() {
		return uneInscription;
	}

	public void setUneInscription(EOCandidatGrhum uneInscription) {
		this.uneInscription = uneInscription;
	}

	public boolean isEditing() {
		return isEditing;
	}

	public boolean isNotEditing() {
		return !isEditing;
	}

	public void setIsEditing(boolean isEditing) {
		this.isEditing = isEditing;
	}

	public NSArray<EOResultat> resultats() {
		if (resultats == null) {
			resultats = EOResultat.getResultatsPourAdmission(edc());
		}
		return resultats;
	}

	public void setResultats(NSArray<EOResultat> resultats) {
		this.resultats = resultats;
	}

	public EOResultat unResultat() {
		return unResultat;
	}

	public void setUnResultat(EOResultat unResultat) {
		this.unResultat = unResultat;
	}

	// public WOActionResults printResultats() {
	// try {
	// if (selectedFormationAdmission == null) {
	// return null;
	// }
	// Integer anneeScol = selectedFormationAdmission.toFwkScolarite_ScolFormationAnnee().fannDebut();
	// Integer dspeCode = selectedFormationAdmission.toFwkScolarite_ScolFormationSpecialisation().fspnKey();
	// String orderByKeys = "";
	// for (EOSortOrdering sort : dgEtudiants().sortOrderings()) {
	// if (orderByKeys.length() > 0) {
	// orderByKeys += ", ";
	// }
	// orderByKeys += sort.key().substring(sort.key().lastIndexOf(".") + 1)
	// + (sort.selector().equals(EOSortOrdering.CompareAscending)
	// || sort.selector().equals(EOSortOrdering.CompareCaseInsensitiveAscending) ? " asc" : " desc");
	// }
	//
	// NSData data = PrintFactory.printResultats(session, null, EOGarnucheParametres.getGarnucheUniversite(edc()),
	// EOGarnucheParametres.getGarnucheVille(edc()), dspeCode, selectedFormationAdmission.fadmNiveau(), anneeScol, orderByKeys,
	// selectedFormationAdmission.toString());
	// String fileName = "liste_resultats_" + anneeScol + ".pdf";
	// if (data == null) {
	// throw new Exception("Impossible d'imprimer la liste des résultats " + fileName);
	// }
	// return PrintFactory.afficherPdf(data, fileName);
	// }
	// catch (Throwable e) {
	// e.printStackTrace();
	// session.addSimpleErrorMessage("Erreur", e.getMessage());
	// return null;
	// }
	// }

}