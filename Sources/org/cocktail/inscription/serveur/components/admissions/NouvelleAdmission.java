/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.inscription.serveur.components.admissions;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.inscription.serveur.components.MyComponent;
import org.cocktail.scolarix.serveur.factory.FactoryEtudiant;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public class NouvelleAdmission extends MyComponent {
	private IEtudiant etudiant = null;
	private EOHistorique historique = null;
	private IPersonne selectedPersonne;

	private boolean isDossierBusy;

	private boolean isEditing;

	public NouvelleAdmission(WOContext context) {
		super(context);
	}

	public void reset() {
		setEtudiant(null);
		setHistorique(null);
		setSelectedPersonne(null);
	}

	public boolean isBusy() {
		return isDossierBusy();
	}

	public WOActionResults creer() {
		saisirNouveauDossier(null);
		return null;
	}

	public WOActionResults selectionner() {
		if (getSelectedPersonne() == null) {
			return null;
		}
		EOIndividu individu = (EOIndividu) getSelectedPersonne();
		if (individu == null) {
			return null;
		}
		saisirNouveauDossier(individu);
		return null;
	}

	private void saisirNouveauDossier(EOIndividu individu) {
		edc().revert();
		edc().invalidateAllObjects();
		IEtudiant etudiant = FactoryEtudiant.createForAdmission(edc(), session().anneeScolaire().fannDebut(), session().garnucheApplication().cRne(),
				individu);
		etudiant.setEtudType(EOEtudiant.ETUD_TYPE_ADMISSION);
		setEtudiant(etudiant);
		setHistorique(etudiant.historique(session().anneeScolaire().fannDebut()));
		setIsEditing(true);
	}

	public IEtudiant etudiant() {
		return etudiant;
	}

	public void setEtudiant(IEtudiant etudiant) {
		this.etudiant = etudiant;
	}

	public EOHistorique historique() {
		return historique;
	}

	public void setHistorique(EOHistorique historique) {
		this.historique = historique;
	}

	public boolean isAfficherDossierEtudiant() {
		return etudiant != null;
	}

	public boolean isDossierBusy() {
		return isDossierBusy;
	}

	public void setIsDossierBusy(boolean isDossierBusy) {
		this.isDossierBusy = isDossierBusy;
	}

	public boolean isEditing() {
		return isEditing;
	}

	public void setIsEditing(boolean isEditing) {
		this.isEditing = isEditing;
	}

	public Integer getUtilisateurPersId() {
		return new Integer(session().connectedUserInfo().persId().intValue());
	}

	public IPersonne getSelectedPersonne() {
		return selectedPersonne;
	}

	public void setSelectedPersonne(IPersonne selectedPersonne) {
		this.selectedPersonne = selectedPersonne;
	}

}