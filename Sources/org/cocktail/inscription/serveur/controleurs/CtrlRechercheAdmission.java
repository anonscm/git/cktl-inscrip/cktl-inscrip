/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.inscription.serveur.controleurs;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.inscription.serveur.components.NouveauDossier;
import org.cocktail.inscription.serveur.components.admissions.RechercheAdmission;
import org.cocktail.inscription.serveur.components.exceptions.CtrlInscriptionException;
import org.cocktail.scolarix.serveur.exception.EtudiantException;
import org.cocktail.scolarix.serveur.interfaces.IEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOCandidatGrhum;
import org.cocktail.scolarix.serveur.metier.eos.EOEtudiant;
import org.cocktail.scolarix.serveur.metier.eos.EOHistorique;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXS;

public class CtrlRechercheAdmission {
	private RechercheAdmission component = null;
	private EOEditingContext edc = null;

	private NSArray<EOCandidatGrhum> candidats = null;
	private EOCandidatGrhum unCandidat;
	private IEtudiant leEtudiant;

	public CtrlRechercheAdmission(RechercheAdmission component) {
		super();
		this.component = component;
		if (component != null) {
			edc = component.edc();
		}
	}

	public WOActionResults search() {
		setLeEtudiant(null);
		try {
			if (component.qbe().allKeys().count() > 0) {
				if (component.qbe().allKeys().count() == 1 && component.qbe().valueForKey("etablissement") != null) {
					return null;
				}
				BigDecimal anneeBG = (BigDecimal) component.qbe().valueForKey("annee");
				BigDecimal etudNumeroBG = (BigDecimal) component.qbe().valueForKey("etudNumero");
				BigDecimal numAdm = (BigDecimal) component.qbe().valueForKey("numAdm");
				NSArray<EOCandidatGrhum> candidats = null;
				if (etudNumeroBG != null) {
					candidats = EOCandidatGrhum.fetchAll(edc, EOCandidatGrhum.ETUD_NUMERO_KEY, new Integer(etudNumeroBG.intValue()),
							ERXS.ascs(EOCandidatGrhum.CAND_NOM_KEY, EOCandidatGrhum.CAND_PRENOM_KEY));
				}
				else {
					if (numAdm != null) {
						candidats = EOCandidatGrhum.fetchAll(edc, EOCandidatGrhum.CAND_NUM_ADM_KEY, new Integer(numAdm.intValue()),
								ERXS.ascs(EOCandidatGrhum.CAND_NOM_KEY, EOCandidatGrhum.CAND_PRENOM_KEY));
					}
					else {
						candidats = EOCandidatGrhum.getCandidats(edc, anneeBG == null ? null : new Integer(anneeBG.intValue()), (String) component
								.qbe().valueForKey("etudCodeIne"), (String) component.qbe().valueForKey("nom"),
								(String) component.qbe().valueForKey("prenom"), (NSTimestamp) component.qbe().valueForKey("dNaissance"), null);
					}
				}
				if (candidats == null || candidats.count() == 0) {
					component.session().addSimpleInfoMessage("Pfff...", "Aucun dossier d'admission trouvé...");
				}
				setCandidats(candidats);
			}
		}
		catch (CtrlInscriptionException e) {
			component.session().addSimpleErrorMessage("Erreur", e.getMessage());
			return null;
		}
		return null;
	}

	public WOActionResults cancel() {
		reset();
		component.qbe().removeAllObjects();
		return null;
	}

	public void reset() {
		leEtudiant = null;
		unCandidat = null;
		candidats = null;
	}

	public WOActionResults afficherLeDossier() {
		setLeEtudiant(getEtudiantForCandidat(unCandidat()));
		return null;
	}

	public WOActionResults inscrire() {
		ERXRedirect redirectPage = null;
		NouveauDossier page;
		try {
			page = (NouveauDossier) component.session().getNewPageWithName(NouveauDossier.class.getName());
		}
		catch (Exception e) {
			page = (NouveauDossier) component.pageWithName(NouveauDossier.class.getName());
		}
		try {
			IEtudiant etudiant = getEtudiantForCandidat(unCandidat());
			etudiant.setEtudType(EOEtudiant.ETUD_TYPE_INSCRIPTION);
			page.initNouveauDossierAdmission(etudiant);
			redirectPage = new ERXRedirect(component.context());
			redirectPage.setComponent(page);
			component.session().setErreur(null);
		}
		catch (CtrlInscriptionException e) {
			component.session().defaultEditingContext().revert();
			component.session().defaultEditingContext().invalidateAllObjects();
			WOResponse response = new WOResponse();
			response.setStatus(500);
			component.session().setErreur(e.getMessageJS());
			return response;
		}
		return redirectPage;
	}

	private IEtudiant getEtudiantForCandidat(EOCandidatGrhum candidat) {
		IEtudiant etudiant = null;
		if (candidat != null) {
			try {
				edc.revert();
				edc.invalidateAllObjects();
				Method m = component.session().finder()
						.getDeclaredMethod("getEtudiantAdmission", new Class[] { EOEditingContext.class, Integer.class });
				etudiant = (IEtudiant) m.invoke(null, new Object[] { component.session().defaultEditingContext(), unCandidat().candNumAdm() });
			}
			catch (SecurityException e1) {
				e1.printStackTrace();
			}
			catch (NoSuchMethodException e2) {
				e2.printStackTrace();
			}
			catch (IllegalArgumentException e3) {
				e3.printStackTrace();
			}
			catch (IllegalAccessException e4) {
				e4.printStackTrace();
			}
			catch (EtudiantException e6) {
				e6.printStackTrace();
			}
			catch (InvocationTargetException e5) {
				e5.printStackTrace();
				if (e5.getCause().getClass().equals(EtudiantException.class)) {
					EtudiantException exception = (EtudiantException) e5.getCause();
					component.session().addSimpleErrorMessage("Erreur", exception.getMessageFormatte());
				}
			}
			if (etudiant != null) {
				etudiant.setRne(component.session().etablissement().toRne());
				NSArray<EtudiantException> userInfos = etudiant.userInfos();
				if (userInfos != null && userInfos.count() > 0) {
					String messages = "";
					Enumeration<EtudiantException> enumUserInfos = userInfos.objectEnumerator();
					while (enumUserInfos.hasMoreElements()) {
						EtudiantException exception = enumUserInfos.nextElement();
						messages += exception.getMessageFormatte() + "\\n";
					}
					component.session().addSimpleErrorMessage("Erreur", messages);
				}
			}
		}
		return etudiant;
	}

	public NSArray<EOCandidatGrhum> candidats() {
		return candidats;
	}

	public void setCandidats(NSArray<EOCandidatGrhum> candidats) {
		this.candidats = candidats;
	}

	public EOCandidatGrhum unCandidat() {
		return unCandidat;
	}

	public void setUnCandidat(EOCandidatGrhum unCandidat) {
		this.unCandidat = unCandidat;
	}

	public IEtudiant leEtudiant() {
		return leEtudiant;
	}

	public void setLeEtudiant(IEtudiant leEtudiant) {
		this.leEtudiant = leEtudiant;
	}

	public EOHistorique leHistorique() {
		if (leEtudiant() != null) {
			return leEtudiant().historique(leEtudiant().anneeInscriptionEnCours());
		}
		return null;
	}
}
